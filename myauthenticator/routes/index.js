var express = require('express');
var router = express.Router();

const homeController = require('../controllers/homeController');

router.get('/home', homeController.GetAll);

module.exports = router;
